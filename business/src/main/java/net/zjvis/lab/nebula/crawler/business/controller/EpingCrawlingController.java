package net.zjvis.lab.nebula.crawler.business.controller;

import com.google.common.collect.Lists;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import net.zjvis.lab.nebula.crawler.business.StandardResponse;
import net.zjvis.lab.nebula.crawler.business.tbt.eping.downloader.EpingJPADownloader;
import net.zjvis.lab.nebula.crawler.business.tbt.gac.ChinaExportMonthlyHSRepository;
import net.zjvis.lab.nebula.crawler.business.tbt.gac.ChinaExportMonthly_HS;
import net.zjvis.lab.nebula.crawler.core.WebCrawler;
import net.zjvis.lab.nebula.crawler.core.component.ApplicationContextUtil;
import net.zjvis.lab.nebula.crawler.core.page.Page;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author AaronY
 * @version 1.0
 * @since 2023/6/25
 */
@Tag(name = "EPING APIs", description = "WTO Eping Crawler API")
@RestController
@RequestMapping("/eping")
public class EpingCrawlingController {

    public static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(EpingCrawlingController.class);
    private static final String DOWNLOAD_API = "/download";
    private static final String CRAWL_API = "/crawl";
    private final ChinaExportMonthlyHSRepository chinaExportMonthlyHSRepository;
    private final EpingJPADownloader epingJPADownloader;

    public EpingCrawlingController(
            EpingJPADownloader epingJPADownloader,
            ChinaExportMonthlyHSRepository chinaExportMonthlyHSRepository
    ) {
        this.epingJPADownloader = epingJPADownloader;
        this.chinaExportMonthlyHSRepository = chinaExportMonthlyHSRepository;
    }

    private final ConcurrentHashMap<String, String> threshold = new ConcurrentHashMap<>();

    @Scheduled(initialDelay = 60, fixedDelay = 6 * 60, timeUnit = TimeUnit.MINUTES)
    @PostMapping(value = DOWNLOAD_API)
    public StandardResponse<String> downloadEpingData() {
        if (threshold.containsKey(DOWNLOAD_API)) {
            return StandardResponse.fail("", new RuntimeException("there is a download task still running."));
        }
        threshold.put(DOWNLOAD_API, Calendar.getInstance().toString());

        Executors.newSingleThreadExecutor().submit(() -> {
            CompletableFuture.supplyAsync(() -> {
                try {
                    epingJPADownloader.run();
                } catch (IOException e) {
                    threshold.remove(DOWNLOAD_API);
                    LOGGER.warn("download failed, since {}", e.getMessage());
                }
                return null;
            }).thenApply(s -> threshold.remove(DOWNLOAD_API)).get();
            return null;
        });
        return StandardResponse.ok();
    }

    @PostMapping(value = CRAWL_API)
    public StandardResponse<String> crawlEpingDocuments() throws IOException {
        if (threshold.containsKey(CRAWL_API)) {
            return StandardResponse.fail("", new RuntimeException("there is a crawler task still running."));
        }
        WebCrawler webCrawler = ApplicationContextUtil.webCrawler();
        List<Page> rootPages = ApplicationContextUtil.rootPages();
        threshold.put(CRAWL_API, Calendar.getInstance().toString());
        Executors.newSingleThreadExecutor().submit(() -> {
            CompletableFuture.supplyAsync(() -> {
                webCrawler.crawl(rootPages);
                return null;
            }).thenApply(s -> threshold.remove(CRAWL_API)).get();
            return null;
        });
        return StandardResponse.ok();
    }

    @Scheduled(initialDelay = 60, fixedDelay = 15 * 60, timeUnit = TimeUnit.SECONDS)
    @Operation(description = "only for backend use to trigger")
    @PostMapping(value = "/trigger")
    public StandardResponse<Void> trigger() throws IOException {
        LOGGER.info("[Auto Trigger] start to trigger API /crawl");
        this.crawlEpingDocuments();
        return StandardResponse.ok();
    }

    @Deprecated
    @Operation(description = "only for backend use to mock data")
    @PostMapping(value = "/persistence")
    public StandardResponse<Void> persistence(String rawHTML, @RequestParam(defaultValue = "true") Boolean isUSD) {
        Document document = Jsoup.parse(rawHTML.replaceAll("\n", ""), "http://stats.customs.gov.cn/");
        List<String> receivedData = document.select("div.th-line").stream()
                .map(element -> {
                    if (element.hasAttr("title")) {
                        return element.attr("title");
                    } else {
                        return element.childNode(0).toString().replaceAll("\\\\s*|\\t|\\r|\\n", "");
                    }
                }).collect(Collectors.toList());
        List<ChinaExportMonthly_HS> pojo = Lists.partition(receivedData, 10).stream().map(cells -> ChinaExportMonthly_HS.builder()
                .statMonth(cells.get(0))
                .hsCodeType(cells.get(1).length())
                .hsCode(cells.get(1))
                .productName(cells.get(2))
                .tradePartnerId(cells.get(3))
                .tradePartnerName(cells.get(4))
                .tradeMethodId(cells.get(5))
                .tradeMethod(cells.get(6))
                .registrationPlaceId(cells.get(7))
                .registrationPlace(cells.get(8))
                .moneyRMB(!isUSD ? Long.parseLong(cells.get(9).replaceAll(",", "")) : -1L)
                .moneyUSD(isUSD ? Long.parseLong(cells.get(9).replaceAll(",", "")) : -1L)
                .build()).collect(Collectors.toList());
        chinaExportMonthlyHSRepository.saveAll(pojo);
        return StandardResponse.ok();
    }
}
