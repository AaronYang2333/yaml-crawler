package net.zjvis.lab.nebula.crawler.business;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.jackson.Jacksonized;

@ToString
@EqualsAndHashCode
public class FileInfo {
    @Getter
    private final String filename;
    @Getter
    private final String url;

    @Builder
    @Jacksonized
    public FileInfo(String filename, String url) {
        this.filename = filename;
        this.url = url;
    }
}
