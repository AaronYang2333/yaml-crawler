package net.zjvis.lab.nebula.crawler.business;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;


@OpenAPIDefinition(info = @Info(title = "Nebula Crawler API", description = "", version = "v0.1"))
@EnableScheduling
@SpringBootApplication(scanBasePackages = {
        "net.zjvis.lab.nebula.crawler.core",
        "net.zjvis.lab.nebula.crawler.business"})
@EnableJpaRepositories(basePackages = {"net.zjvis.lab.nebula.crawler.core", "net.zjvis.lab.nebula.crawler.business"})
@EntityScan(basePackages = {"net.zjvis.lab.nebula.crawler.core", "net.zjvis.lab.nebula.crawler.business"})
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}