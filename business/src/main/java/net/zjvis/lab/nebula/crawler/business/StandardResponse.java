package net.zjvis.lab.nebula.crawler.business;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import okhttp3.Response;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

@Builder
@Getter
@AllArgsConstructor
public class StandardResponse<DataType> implements Serializable {
    private static final long serialVersionUID = -5740273676686389382L;
    @Builder.Default
    private long code = Code.SUCCESS.getValue();
    private DataType data;
    private String message;

    public static <DataType> StandardResponse<DataType> ok() {
        return StandardResponse.<DataType>builder().build();
    }

    public static <DataType> StandardResponse<DataType> ok(DataType result) {
        return StandardResponse.<DataType>builder().data(result).build();
    }

    public static <DataType> StandardResponse<DataType> fail(DataType result, Exception exception) {
        return StandardResponse.<DataType>builder().code(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .message(exception.getMessage()).build();
    }

    public static <DataType> StandardResponse<DataType> fail(DataType result, HttpStatus httpStatus,
                                                             Exception exception) {
        return StandardResponse.<DataType>builder().code(httpStatus.value())
                .message(exception.getMessage()).build();
    }

    @Deprecated
    public static <DataType> StandardResponse<DataType> fail(
            Response response,
            Class<DataType> resultType) {
        return StandardResponse.<DataType>builder().code(response.code())
                .message(response.message())
                .build();
    }

    @Deprecated
    public static <DataType> StandardResponse<DataType> failInternalServerError(String message) {
        return StandardResponse.<DataType>builder()
                .code(HttpStatus.INTERNAL_SERVER_ERROR.ordinal())
                .message(message)
                .build();
    }

    @Deprecated
    public static <DataType> StandardResponse<DataType> failBadRequest(String message) {
        return StandardResponse.<DataType>builder()
                .code(HttpStatus.BAD_REQUEST.ordinal())
                .message(message)
                .build();
    }

    public static <DataType> StandardResponse<DataType> failNotFound(String message) {
        return StandardResponse.<DataType>builder()
            .code(HttpStatus.NOT_FOUND.ordinal())
            .message(message)
            .build();
    }

    public static  <T> StandardResponse<T> wrapper(Exception e, String message, int regionCode, int businessCode, Class<T> clazz) {
        return StandardResponse.<T>builder()
                .code(Code.combine(Code.BAD_REQUEST, regionCode, businessCode))
                .message(message)
                .build();
    }
}
