package net.zjvis.lab.nebula.crawler.core.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.extern.jackson.Jacksonized;
import okhttp3.OkHttpClient;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Getter
public class DownloadPage<DataType> extends AbstractPage implements HasIdentity<DownloadPage.ID> {
    @Getter
    @EqualsAndHashCode
    @ToString
    public static class ID {
        private final String application;
        private final String groupKey;
        private final String entityKey;

        @Builder
        public ID(
                String application,
                String groupKey,
                String entityKey
        ) {
            this.application = application;
            this.groupKey = groupKey;
            this.entityKey = entityKey;
        }
    }

    @JsonProperty
    private final String application;
    @JsonProperty
    private final String groupKey;
    @JsonProperty
    private final String entityKey;
    @JsonProperty
    private final String filename;
    @JsonProperty
    private final DataType data;
    @JsonProperty
    private final Long timestamp;
    @Builder.Default
    @JsonProperty
    private final int weight = 100;

    @Builder
    @Jacksonized
    public DownloadPage(
            @NonNull String url,
            @NonNull String application,
            @NonNull String groupKey,
            String entityKey,
            String filename,
            DataType data,
            Long timestamp,
            OkHttpClient okHttpClient
    ) {
        super(url, okHttpClient);
        this.application = application;
        this.groupKey = groupKey;
        this.entityKey = entityKey;
        this.filename = filename;
        this.data = data;
        this.timestamp = timestamp;
    }

    @Override
    public ID identity() {
        return ID.builder()
                .application(application)
                .groupKey(groupKey)
                .entityKey(entityKey)
                .build();
    }
}
