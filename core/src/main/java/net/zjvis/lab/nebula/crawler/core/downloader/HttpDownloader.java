package net.zjvis.lab.nebula.crawler.core.downloader;

import lombok.Builder;
import net.zjvis.lab.nebula.crawler.core.exception.TimeOutException;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BoundedInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.ConnectException;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class HttpDownloader implements Downloader {
    private static final Logger LOGGER = LoggerFactory.getLogger(HttpDownloader.class);
    private final OkHttpClient okHttpClient;
    private final long limit;

    @Builder
    public HttpDownloader(OkHttpClient okHttpClient, Long limit) {
        this.okHttpClient = null == okHttpClient ? new OkHttpClient().newBuilder()
                .connectTimeout(30, TimeUnit.SECONDS).build() : okHttpClient;
        // default limit: 2Gb
        this.limit = null == limit ? Integer.MIN_VALUE : limit;
    }

    @Override
    public void download(String url, OutputStream outputStream) throws IOException {
        Request request = new Request.Builder()
//                .header("Accept-Encoding", "identity")
                .url(url)
                .build();
        try (Response response = okHttpClient.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                throw new IOException(String.format(
                        "download url(%s) failed with http code %s", url, response.code()));
            }
            Optional.ofNullable(response.body())
                    .ifPresentOrElse(
                            responseBody -> {
                                try (InputStream inputStream = responseBody.byteStream()) {
                                    try (InputStream boundedInputStream = new BoundedInputStream(
                                            responseBody.byteStream(), limit)) {
                                        IOUtils.copy(boundedInputStream, outputStream);
                                        if (inputStream.available() > 0) {
                                            throw new UncheckedIOException(new IOException(
                                                    String.format("limit(%s) exceeds", limit)));
                                        }
                                    }
                                } catch (IOException e) {
                                    throw new UncheckedIOException(e);
                                }
                            },
                            () -> LOGGER.warn("response of url({}) has no body", url)
                    );
        } catch (ConnectException e) {
            throw new TimeOutException("connect time out");
        } catch (Exception e) {
            throw new IOException(String.format("make http request of url(%s) failed. since %s", url, e.getMessage()));
        }
    }

    @Override
    public byte[] download(String url) throws IOException {
        Request request = new Request.Builder().url(url).build();
        try (Response response = okHttpClient.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                throw new RuntimeException(String.format("download url(%s) failed with http code %s", url, response.code()));
            }

            if (null != response.body()) {
                try (InputStream inputStream = response.body().byteStream()) {
                    try (InputStream boundedInputStream = new BoundedInputStream(response.body().byteStream(), limit)) {
                        byte[] allBytes = IOUtils.buffer(boundedInputStream).readAllBytes();
                        if (inputStream.available() > 0) {
                            throw new RuntimeException(String.format("response has exceed the limit(%s)", limit));
                        }
                        return allBytes;
                    }
                } catch (Exception e) {
                    throw new RuntimeException(String.format("parse http response of url(%s) failed. since %s", url, e.getMessage()));
                }
            } else {
                throw new RuntimeException(String.format("response of url(%s) has no body", url));
            }
        } catch (ConnectException e) {
            throw new TimeOutException("connect time out");
        } catch (Exception e) {
            throw new RuntimeException(String.format("make http request of url(%s) failed. since %s", url, e.getMessage()));
        }
    }
}
