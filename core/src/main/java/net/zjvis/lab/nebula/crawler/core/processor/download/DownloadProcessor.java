package net.zjvis.lab.nebula.crawler.core.processor.download;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;
import lombok.extern.jackson.Jacksonized;
import net.zjvis.lab.nebula.crawler.core.page.DownloadPage;
import net.zjvis.lab.nebula.crawler.core.page.Page;
import net.zjvis.lab.nebula.crawler.core.page.persist.PagePersist;
import net.zjvis.lab.nebula.crawler.core.page.persist.entity.DownloadPageEntity;
import net.zjvis.lab.nebula.crawler.core.processor.AbstractProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class DownloadProcessor<DataType> extends AbstractProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(DownloadProcessor.class);
    @JsonProperty
    private final PagePersist<DownloadPage.ID> pagePersist;
    @JsonProperty
    private final DownloadPageTransformer<DataType> downloadPageTransformer;
    @JsonProperty
    private final SkipExists<DataType> skipExists;

    @Builder(toBuilder = true)
    @Jacksonized
    public DownloadProcessor(
            @NonNull PagePersist<DownloadPage.ID> pagePersist,
            @NonNull DownloadPageTransformer<DataType> downloadPageTransformer,
            @NonNull SkipExists<DataType> skipExists
    ) {
        this.pagePersist = pagePersist;
        this.downloadPageTransformer = downloadPageTransformer;
        this.skipExists = skipExists;
    }

    @Override
    public boolean canProcess(Page page) {
        return !page.processed()
                && page instanceof DownloadPage;
    }

    @Override
    protected Set<Page> doProcess(Page page) throws Exception {
        final DownloadPage<DataType> downloadPage = page.unwrap(DownloadPage.class);
        if (skipExists.skip(downloadPage, pagePersist, downloadPageTransformer)) {
            LOGGER.info("skip existing page: {}", page);
            return Collections.emptySet();
        }
        try {
            pagePersist.upsert(downloadPageTransformer.apply(downloadPage));
        }catch (UncheckedIOException e){
            pagePersist.upsert(DownloadPageEntity.uncheckedFrom(downloadPage)
                    .toBuilder()
                    .fileContent("404 file".getBytes(StandardCharsets.UTF_8))
                    .build());
        }
        return Collections.emptySet();
    }
}
