package net.zjvis.lab.nebula.crawler.core.page.persist;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.google.common.base.Preconditions;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.zjvis.lab.nebula.crawler.core.page.DownloadPage;
import net.zjvis.lab.nebula.crawler.core.page.HasIdentity;
import net.zjvis.lab.nebula.crawler.core.page.persist.entity.DownloadPageEntity;

@JsonTypeInfo(property = "@class", use = JsonTypeInfo.Id.CLASS)
@EqualsAndHashCode
@ToString
public abstract class DownloadPagePersist implements PagePersist<DownloadPage.ID> {
    protected DownloadPageEntity unwrap(HasIdentity<DownloadPage.ID> page) {
        Preconditions.checkArgument(
                page instanceof DownloadPageEntity,
                String.format("instance(class=%s) is not instance of %s",
                        page.getClass(), DownloadPageEntity.class.getName())
        );
        return (DownloadPageEntity) page;
    }
}
