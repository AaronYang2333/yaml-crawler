package net.zjvis.lab.nebula.crawler.core.processor.download;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import net.zjvis.lab.nebula.crawler.core.page.DatasetPage;
import net.zjvis.lab.nebula.crawler.core.page.DownloadPage;
import net.zjvis.lab.nebula.crawler.core.page.persist.PagePersist;
import org.springframework.data.repository.CrudRepository;

@JsonTypeInfo(property = "@class", use = JsonTypeInfo.Id.CLASS)
public interface SkipExists<DataType> {
    boolean skip(
            DownloadPage<DataType> downloadPage,
            PagePersist<DownloadPage.ID> pagePersist,
            DownloadPageTransformer<DataType> downloadPageTransformer
    );

    boolean skip(
            DatasetPage<DataType> datasetPage,
            CrudRepository<DataType, ?> abstractRepository
    );
}
