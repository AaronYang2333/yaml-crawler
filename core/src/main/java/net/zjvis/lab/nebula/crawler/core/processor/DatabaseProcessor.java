package net.zjvis.lab.nebula.crawler.core.processor;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.jackson.Jacksonized;
import net.zjvis.lab.nebula.crawler.core.component.ApplicationContextUtil;
import net.zjvis.lab.nebula.crawler.core.page.DatasetPage;
import net.zjvis.lab.nebula.crawler.core.page.Page;
import net.zjvis.lab.nebula.crawler.core.processor.download.SkipExists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;

import java.util.Collections;
import java.util.Set;

/**
 * @author AaronY
 * @version 1.0
 * @since 2023/6/14
 */
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class DatabaseProcessor<DataType, ID> extends AbstractProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseProcessor.class);

    @JsonProperty
    private final String datasetRepositoryBeanName;
    @JsonProperty
    private final SkipExists<DataType> skipExists;

    private transient CrudRepository<DataType, ID> abstractRepository;


    @Jacksonized
    public DatabaseProcessor(String datasetRepositoryBeanName,
                             SkipExists<DataType> skipExists) {
        this.datasetRepositoryBeanName = datasetRepositoryBeanName;
        this.skipExists = skipExists;
    }

    @Override
    public boolean canProcess(Page page) {
        return !page.processed()
                && page instanceof DatasetPage;
    }

    @Override
    protected Set<Page> doProcess(Page page) throws Exception {
        final DatasetPage<DataType> datasetPage = page.unwrap(DatasetPage.class);
//        if (skipExists.skip(datasetPage, crudRepository)) {
//            LOGGER.info("skip existing dataset record: {}", datasetPage);
//            return Collections.emptySet();
//        }
        if (page instanceof DatasetPage) {
            try {
                datasetRepository().save(datasetPage.getData());
            } catch (Exception e) {
                LOGGER.error("saving record failed, since {}", e.getMessage());
            }
        }
        return Collections.emptySet();
    }


    private CrudRepository<DataType, ID> datasetRepository() {
        if (null != datasetRepositoryBeanName) {
            abstractRepository = ApplicationContextUtil.getBean(datasetRepositoryBeanName, CrudRepository.class);
        }
        return abstractRepository;
    }
}
