package net.zjvis.lab.nebula.crawler.core.exception;

public class BugException extends RuntimeException {
    public BugException(String message) {
        super(message);
    }

    public BugException(String message, Throwable cause) {
        super(message, cause);
    }
}
