package net.zjvis.lab.nebula.crawler.core.processor.decorator;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.jackson.Jacksonized;
import net.zjvis.lab.nebula.crawler.core.page.Page;
import net.zjvis.lab.nebula.crawler.core.processor.Processor;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ExceptionRecord extends AbstractProcessorDecorator {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    @JsonProperty
    private final File processingErrorLogFile;

    @Builder(toBuilder = true)
    @Jacksonized
    public ExceptionRecord(Processor processor, File processingErrorLogFile) {
        super(processor);
        try {
            this.processingErrorLogFile = null == processingErrorLogFile
                    ? File.createTempFile("processing.", ".error.log")
                    : processingErrorLogFile;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public Set<Page> process(Page page) throws Exception {
        try {
            return super.process(page);
        } catch (Exception e) {
            FileUtils.writeStringToFile(
                    processingErrorLogFile,
                    String.format("processing # %s # failed for # %s \n",
                            OBJECT_MAPPER.writeValueAsString(page), e.getMessage()),
                    StandardCharsets.UTF_8,
                    true
            );
            throw e;
        }
    }
}
