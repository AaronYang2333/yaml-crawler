package net.zjvis.lab.nebula.crawler.core.processor.download;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.jackson.Jacksonized;
import net.zjvis.lab.nebula.crawler.core.page.DatasetPage;
import net.zjvis.lab.nebula.crawler.core.page.DownloadPage;
import net.zjvis.lab.nebula.crawler.core.page.HasIdentity;
import net.zjvis.lab.nebula.crawler.core.page.persist.PagePersist;
import org.springframework.data.repository.CrudRepository;

import java.util.Objects;

@Builder
@EqualsAndHashCode
@Jacksonized
@ToString
public class SkipExistsById<DataType> implements SkipExists<DataType> {
    @Override
    public boolean skip(
            DownloadPage<DataType> downloadPage,
            PagePersist<DownloadPage.ID> pagePersist,
            DownloadPageTransformer<DataType> downloadPageTransformer
    ) {
        DownloadPage.ID id = downloadPage.identity();
        return Objects.equals(
                id,
                pagePersist.findById(id)
                        .map(HasIdentity::identity)
                        .orElse(null)
        );
    }

    @Override
    public boolean skip(DatasetPage<DataType> datasetPage, CrudRepository<DataType, ?> abstractRepository) {
        DatasetPage.ID identity = datasetPage.identity();
//        abstractRepository.

        return false;
    }

}
