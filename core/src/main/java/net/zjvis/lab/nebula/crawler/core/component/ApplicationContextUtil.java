package net.zjvis.lab.nebula.crawler.core.component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import net.zjvis.lab.nebula.crawler.core.WebCrawler;
import net.zjvis.lab.nebula.crawler.core.page.Page;
import net.zjvis.lab.nebula.crawler.core.entity.ProxyEntity;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;

@Component
public class ApplicationContextUtil implements ApplicationContextAware {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper(YAMLFactory.builder()
            .disable(YAMLGenerator.Feature.USE_NATIVE_TYPE_ID)
            .build());
    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(@NotNull ApplicationContext applicationContext) throws BeansException {
        ApplicationContextUtil.applicationContext = applicationContext;
    }

    public static <T> T getBean(String name, Class<T> clazz) {
        Object bean = applicationContext.getBean(name);
        if (!clazz.isInstance(bean)) {
            throw new RuntimeException(String.format(
                    "bean(class=%s) is not an instance of class(%s)", bean.getClass().getName(), clazz.getName()));
        }
        return clazz.cast(bean);
    }

    public static <T> T getBean(Class<T> clazz) {
        Object bean = applicationContext.getBean(clazz);
        if (!clazz.isInstance(bean)) {
            throw new RuntimeException(String.format(
                    "bean(class=%s) is not an instance of class(%s)", bean.getClass().getName(), clazz.getName()));
        }
        return clazz.cast(bean);
    }

    public static WebCrawler webCrawler() throws IOException {
        String webCrawlerConfigurationPath
                = applicationContext.getBean("webCrawlerConfigurationPath", String.class);
        return OBJECT_MAPPER.readValue(
                FileUtils.readFileToString(new File(webCrawlerConfigurationPath), StandardCharsets.UTF_8),
                new TypeReference<>() {
                });
    }

    public static List<Page> rootPages() throws IOException {
        String webCrawlerRootPagesPath
                = applicationContext.getBean("webCrawlerRootPagesPath", String.class);
        return OBJECT_MAPPER.readValue(
                FileUtils.readFileToString(new File(webCrawlerRootPagesPath), StandardCharsets.UTF_8),
                new TypeReference<>() {
                });
    }

    public static Optional<ProxyEntity> randomProxy() {
        ProxyManager proxyManager = applicationContext.getBean("proxyManager", ProxyManager.class);
        return proxyManager.randomProxy();
    }

    public static void removeProxy(ProxyEntity proxy) {
        ProxyManager proxyManager = applicationContext.getBean("proxyManager", ProxyManager.class);
        proxyManager.remove(proxy);
    }
}
