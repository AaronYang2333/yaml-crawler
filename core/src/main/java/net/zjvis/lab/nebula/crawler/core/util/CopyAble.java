package net.zjvis.lab.nebula.crawler.core.util;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.UncheckedIOException;

@JsonTypeInfo(property = "@class", use = JsonTypeInfo.Id.CLASS)
public interface CopyAble {
    ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    default <T extends CopyAble> T copy() {
        try {
            return OBJECT_MAPPER.readValue(
                    OBJECT_MAPPER.writeValueAsString(this),
                    new TypeReference<>() {
                    }
            );
        } catch (JsonProcessingException e) {
            throw new UncheckedIOException(e);
        }
    }
}
