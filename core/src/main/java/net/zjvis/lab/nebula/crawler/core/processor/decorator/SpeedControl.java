package net.zjvis.lab.nebula.crawler.core.processor.decorator;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.jackson.Jacksonized;
import net.zjvis.lab.nebula.crawler.core.page.Page;
import net.zjvis.lab.nebula.crawler.core.processor.Processor;

import java.util.Set;
import java.util.concurrent.TimeUnit;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SpeedControl extends AbstractProcessorDecorator {
    @JsonProperty
    private final long time;
    @JsonProperty
    private final TimeUnit unit;

    @Builder(toBuilder = true)
    @Jacksonized
    public SpeedControl(Processor processor, Long time, TimeUnit unit) {
        super(processor);
        this.time = null != time ? time : 1L;
        this.unit = null != unit ? unit : TimeUnit.SECONDS;
    }

    @Override
    public Set<Page> process(Page page) throws Exception {
        Set<Page> pageSet = super.process(page);
        unit.sleep(time);
        return pageSet;
    }
}
