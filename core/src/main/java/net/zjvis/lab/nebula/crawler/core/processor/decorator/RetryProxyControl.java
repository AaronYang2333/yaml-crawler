package net.zjvis.lab.nebula.crawler.core.processor.decorator;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;
import lombok.extern.jackson.Jacksonized;
import net.zjvis.lab.nebula.crawler.core.component.ApplicationContextUtil;
import net.zjvis.lab.nebula.crawler.core.exception.BugException;
import net.zjvis.lab.nebula.crawler.core.exception.NotProxyException;
import net.zjvis.lab.nebula.crawler.core.exception.TimeOutException;
import net.zjvis.lab.nebula.crawler.core.page.AbstractPage;
import net.zjvis.lab.nebula.crawler.core.page.HttpPage;
import net.zjvis.lab.nebula.crawler.core.page.Page;
import net.zjvis.lab.nebula.crawler.core.processor.Processor;
import net.zjvis.lab.nebula.crawler.core.entity.ProxyEntity;
import okhttp3.*;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.IOException;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static java.lang.Integer.parseInt;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class RetryProxyControl extends AbstractProcessorDecorator {

    @JsonProperty
    private final Integer retryTimes;
    private transient Exception lastException;

    @Builder(toBuilder = true)
    @Jacksonized
    public RetryProxyControl(Processor processor, @NonNull Integer retryTimes) {
        super(processor);
        this.retryTimes = retryTimes;
    }

    @Override
    public Set<Page> process(Page page) throws Exception {
        if (!(page instanceof HttpPage)) {
            throw new RuntimeException(new BugException("page is not httpPage"));
        }
        if (!(page instanceof AbstractPage)) {
            throw new RuntimeException(new BugException("page is not abstractPage"));
        }
        for (int index = 0; index < retryTimes; index++) {
            Optional<ProxyEntity> randomElementOptional = ApplicationContextUtil.randomProxy();
            if (randomElementOptional.isEmpty()) {
                throw new NotProxyException("no proxy available");
            }
            ProxyEntity randomElement = randomElementOptional.get();
            String ip = randomElement.getHost();
            String port = randomElement.getPort();
            String username = randomElement.getUsername();
            String password = randomElement.getPassword();
            try {
                Authenticator proxyAuthenticator = (route, response) -> {
                    String credential = Credentials.basic(username, password);
                    return response.request().newBuilder()
                            .header("Proxy-Authorization", credential)
                            .build();
                };
                Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(InetAddress.getByName(ip), parseInt(port)));
                ((AbstractPage) page).setOkHttpClient(new OkHttpClient.Builder()
                        .proxy(proxy)
                        .connectTimeout(100, TimeUnit.SECONDS)
                        .writeTimeout(100, TimeUnit.SECONDS)
                        .readTimeout(100, TimeUnit.SECONDS)
                        .proxyAuthenticator(proxyAuthenticator)
                        .authenticator(proxyAuthenticator)
                        .build());
                return super.process(page);
            } catch (TimeOutException e) {
                ApplicationContextUtil.removeProxy(randomElement);
            } catch (Exception exception) {
                lastException = exception;
            }
        }
        throw new Exception(String.format("process failed with retry times(%s) and last exception(%s)",
                retryTimes, ExceptionUtils.getStackTrace(lastException)
        ));
    }
}
