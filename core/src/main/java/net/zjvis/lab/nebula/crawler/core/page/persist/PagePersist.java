package net.zjvis.lab.nebula.crawler.core.page.persist;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import net.zjvis.lab.nebula.crawler.core.page.HasIdentity;
import net.zjvis.lab.nebula.crawler.core.util.CopyAble;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

@JsonTypeInfo(property = "@class", use = JsonTypeInfo.Id.CLASS)
public interface PagePersist<IdentityType> extends CopyAble {
    HasIdentity<IdentityType> save(HasIdentity<IdentityType> page);

    void deleteById(IdentityType id);

    HasIdentity<IdentityType> upsert(HasIdentity<IdentityType> page);

    Optional<HasIdentity<IdentityType>> findById(IdentityType id);

    Iterable<HasIdentity<IdentityType>> findAll(Pageable pageable);
}
