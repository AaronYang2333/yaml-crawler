package net.zjvis.lab.nebula.crawler.core;

import net.zjvis.lab.nebula.crawler.core.component.ApplicationContextUtil;
import net.zjvis.lab.nebula.crawler.core.component.ProxyManager;
import net.zjvis.lab.nebula.crawler.core.page.persist.DownloadPageDatabasePersist;
import net.zjvis.lab.nebula.crawler.core.repository.DownloadPageRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.domain.PageRequest;

import java.io.IOException;

@SpringBootApplication
@EnableConfigurationProperties(ProxyManager.class)
public class Application {
    public static void main(String[] args) throws IOException {
        SpringApplication.run(Application.class, args);
        ApplicationContextUtil.webCrawler()
                .crawl(ApplicationContextUtil.rootPages());
        DownloadPageDatabasePersist.builder()
                .downloadPageRepositoryBeanName(DownloadPageRepository.BEAN_NAME)
                .build()
                .findAll(PageRequest.of(0, Integer.MAX_VALUE))
                .forEach(downloadPageEntity -> System.out.println(downloadPageEntity));
    }
}
