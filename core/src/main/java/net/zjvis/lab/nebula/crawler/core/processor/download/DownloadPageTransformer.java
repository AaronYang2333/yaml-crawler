package net.zjvis.lab.nebula.crawler.core.processor.download;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import net.zjvis.lab.nebula.crawler.core.page.DownloadPage;
import net.zjvis.lab.nebula.crawler.core.page.HasIdentity;
import net.zjvis.lab.nebula.crawler.core.util.CopyAble;

import java.util.function.Function;

@JsonTypeInfo(property = "@class", use = JsonTypeInfo.Id.CLASS)
public interface DownloadPageTransformer<DataType>
        extends Function<DownloadPage<DataType>, HasIdentity<DownloadPage.ID>>, CopyAble {
    @Override
    HasIdentity<DownloadPage.ID> apply(DownloadPage<DataType> downloadPage);
}