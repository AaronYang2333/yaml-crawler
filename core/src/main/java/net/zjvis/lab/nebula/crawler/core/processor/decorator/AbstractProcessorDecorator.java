package net.zjvis.lab.nebula.crawler.core.processor.decorator;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.zjvis.lab.nebula.crawler.core.page.Page;
import net.zjvis.lab.nebula.crawler.core.processor.Processor;

import java.util.Set;

@EqualsAndHashCode
@ToString
public abstract class AbstractProcessorDecorator implements Processor {
    @JsonProperty
    protected final Processor processor;

    public AbstractProcessorDecorator(Processor processor) {
        if (null == processor) {
            throw new RuntimeException("processor not set");
        }
        this.processor = processor;
    }

    @Override
    public Set<Page> process(Page page) throws Exception {
        return processor.process(page);
    }

    @Override
    public boolean canProcess(Page page) {
        return processor.canProcess(page);
    }
}
