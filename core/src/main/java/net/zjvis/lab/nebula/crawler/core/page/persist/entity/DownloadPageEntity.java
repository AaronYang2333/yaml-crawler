package net.zjvis.lab.nebula.crawler.core.page.persist.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.*;
import lombok.extern.jackson.Jacksonized;
import net.zjvis.lab.nebula.crawler.core.page.DownloadPage;
import net.zjvis.lab.nebula.crawler.core.page.HasIdentity;

import javax.persistence.*;
import java.io.UncheckedIOException;

@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"application", "groupKey", "entityKey"})})
@Builder(toBuilder = true)
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Jacksonized
@ToString
public class DownloadPageEntity implements HasIdentity<DownloadPage.ID> {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String url;
    private String application;
    private String groupKey;
    private String entityKey;
    private String filename;
    @ToString.Exclude
    @Lob
    private String dataJson;
    @ToString.Exclude
    @Lob
    @JsonIgnore
    private byte[] fileContent;
    private Long timestamp;

    public static <DataType> DownloadPageEntity from(
            DownloadPage<DataType> downloadPage
    ) throws JsonProcessingException {
        return DownloadPageEntity.builder()
                .url(downloadPage.getUrl())
                .application(downloadPage.getApplication())
                .groupKey(downloadPage.getGroupKey())
                .entityKey(downloadPage.getEntityKey())
                .filename(downloadPage.getFilename())
                .dataJson(OBJECT_MAPPER.writeValueAsString(downloadPage.getData()))
                .timestamp(downloadPage.getTimestamp())
                .build();
    }

    public static <DataType> DownloadPageEntity uncheckedFrom(DownloadPage<DataType> downloadPage) {
        try {
            return from(downloadPage);
        } catch (JsonProcessingException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public DownloadPage.ID identity() {
        return DownloadPage.ID.builder()
                .application(application)
                .groupKey(groupKey)
                .entityKey(entityKey)
                .build();
    }
}
