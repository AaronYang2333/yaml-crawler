package net.zjvis.lab.nebula.crawler.core.repository;

import net.zjvis.lab.nebula.crawler.core.page.persist.entity.DownloadPageEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository(DownloadPageRepository.BEAN_NAME)
public interface DownloadPageRepository extends PagingAndSortingRepository<DownloadPageEntity, Long> {
    String BEAN_NAME = "downloadPageRepository";

    @Transactional
    Optional<DownloadPageEntity> findByApplicationAndGroupKeyAndEntityKey(
            String application, String groupKey, String entityKey);

    @Transactional
    void deleteByApplicationAndGroupKeyAndEntityKey(
            String application, String groupKey, String entityKey);
}
