package net.zjvis.lab.nebula.crawler.core.page.persist;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.jackson.Jacksonized;
import net.zjvis.lab.nebula.crawler.core.page.DownloadPage;
import net.zjvis.lab.nebula.crawler.core.page.HasIdentity;
import net.zjvis.lab.nebula.crawler.core.page.persist.entity.DownloadPageEntity;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.AbstractFileFilter;
import org.apache.commons.io.filefilter.FalseFileFilter;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Pageable;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class DownloadPageFileSystemPersist extends DownloadPagePersist {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final String ENTITY_JSON_FILENAME = "entity.json";
    @JsonProperty
    private final File baseDirectory;

    @Builder(toBuilder = true)
    @Jacksonized
    public DownloadPageFileSystemPersist(File baseDirectory) {
        this.baseDirectory = baseDirectory;
    }

    @Override
    public HasIdentity<DownloadPage.ID> save(HasIdentity<DownloadPage.ID> page) {
        DownloadPage.ID identity = page.identity();
        String application = identity.getApplication();
        String groupKey = identity.getGroupKey();
        String entityKey = identity.getEntityKey();
        File directory = constructDirectory(application, groupKey, entityKey);
        File entityJsonFile = concatFilename(directory, ENTITY_JSON_FILENAME);
        DownloadPageEntity downloadPageEntity = unwrap(page);
        File contentFile = concatFilename(directory, downloadPageEntity.getFilename());
        try {
            FileUtils.forceMkdir(directory);
            FileUtils.writeStringToFile(
                    entityJsonFile,
                    OBJECT_MAPPER.writeValueAsString(downloadPageEntity),
                    StandardCharsets.UTF_8);
            FileUtils.writeByteArrayToFile(
                    contentFile,
                    downloadPageEntity.getFileContent());
            return downloadPageEntity;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public void deleteById(DownloadPage.ID id) {
        File directory = constructDirectory(id.getApplication(), id.getGroupKey(), id.getEntityKey());
        FileUtils.deleteQuietly(directory);
    }

    @Override
    public HasIdentity<DownloadPage.ID> upsert(HasIdentity<DownloadPage.ID> page) {
        return save(page);
    }

    @Override
    public Optional<HasIdentity<DownloadPage.ID>> findById(DownloadPage.ID id) {
        String application = id.getApplication();
        String groupKey = id.getGroupKey();
        String entityKey = id.getEntityKey();
        File directory = constructDirectory(application, groupKey, entityKey);
        File entityJsonFile = concatFilename(directory, ENTITY_JSON_FILENAME);
        return constructEntity(directory, entityJsonFile)
                .map(entity -> entity);
    }

    @Override
    public Iterable<HasIdentity<DownloadPage.ID>> findAll(Pageable pageable) {
        Iterator<File> directoryIterator = FileUtils.iterateFiles(
                baseDirectory,
                FalseFileFilter.INSTANCE,
                new AbstractFileFilter() {
                    @Override
                    public boolean accept(File directory) {
                        return Objects.equals(directory.getParentFile().getParentFile(), baseDirectory);
                    }
                });
        return StreamSupport.stream(
                        Spliterators.spliteratorUnknownSize(directoryIterator, Spliterator.ORDERED),
                        false
                ).sorted(Comparator.comparing(File::getAbsoluteFile))
                .skip(pageable.getOffset())
                .limit(pageable.getPageSize())
                .map(directory -> constructEntity(
                        directory,
                        concatFilename(directory, ENTITY_JSON_FILENAME)))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    @NotNull
    private Optional<DownloadPageEntity> constructEntity(File directory, File entityJsonFile) {
        if (!entityJsonFile.exists()) {
            return Optional.empty();
        }
        DownloadPageEntity entity = readEntityFromJsonFile(entityJsonFile);
        File contentFile = concatFilename(directory, entity.getFilename());
        try {
            return Optional.of(
                    entity.toBuilder()
                            .fileContent(FileUtils.readFileToByteArray(contentFile))
                            .build()
            );
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private DownloadPageEntity readEntityFromJsonFile(File entityJsonFile) {
        DownloadPageEntity entity;
        try {
            entity = OBJECT_MAPPER.readValue(
                    FileUtils.readFileToString(entityJsonFile, StandardCharsets.UTF_8),
                    new TypeReference<>() {
                    }
            );
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        return entity;
    }

    @NotNull
    private File constructDirectory(String application, String groupKey, String entityKey) {
        return new File(FilenameUtils.concat(
                application,
                FilenameUtils.concat(
                        FilenameUtils.concat(
                                baseDirectory.getAbsolutePath(), groupKey),
                        entityKey
                )
        ));
    }

    private File concatFilename(File directory, String fullFilenameToAdd) {
        return new File(FilenameUtils.concat(
                directory.getAbsolutePath(), fullFilenameToAdd));
    }
}
