package net.zjvis.lab.nebula.crawler.core.component;

import lombok.Getter;
import net.zjvis.lab.nebula.crawler.core.entity.ProxyEntity;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.Random;

@Component
@ConfigurationProperties(prefix = "application.proxy")
public class ProxyManager {
    private static final Random RANDOM = new Random();
    @Getter
    private final List<ProxyEntity> pool;

    public ProxyManager(List<ProxyEntity> pool) {
        this.pool = pool;
    }

    public synchronized Optional<ProxyEntity> randomProxy() {
        if (pool.isEmpty()){
            return Optional.empty();
        }
        return Optional.of( pool.get(RANDOM.nextInt(pool.size())));
    }

    public synchronized void remove(ProxyEntity proxy) {
        pool.remove(proxy);
    }
}
