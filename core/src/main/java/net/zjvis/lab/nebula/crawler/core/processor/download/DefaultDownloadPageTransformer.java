package net.zjvis.lab.nebula.crawler.core.processor.download;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.jackson.Jacksonized;
import net.zjvis.lab.nebula.crawler.core.downloader.Downloader;
import net.zjvis.lab.nebula.crawler.core.downloader.HttpDownloader;
import net.zjvis.lab.nebula.crawler.core.page.DownloadPage;
import net.zjvis.lab.nebula.crawler.core.page.persist.entity.DownloadPageEntity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UncheckedIOException;

@Builder
@EqualsAndHashCode
@Jacksonized
@ToString
public class DefaultDownloadPageTransformer<DataType>
        implements DownloadPageTransformer<DataType> {
    private static final Downloader DOWNLOADER = HttpDownloader.builder().build();

    @Override
    public DownloadPageEntity apply(DownloadPage<DataType> downloadPage) {
        return DownloadPageEntity.uncheckedFrom(downloadPage)
                .toBuilder()
                .fileContent(
                        null == downloadPage.getFilename()? null :uncheckedDownloadFileContent(downloadPage.getUrl()))
                .build();
    }

    private static byte[] uncheckedDownloadFileContent(String url) {
        try {
            return downloadFileContent(url);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private static byte[] downloadFileContent(String url) throws IOException {
        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
            DOWNLOADER.download(url, byteArrayOutputStream);
            return byteArrayOutputStream.toByteArray();
        }
    }
}