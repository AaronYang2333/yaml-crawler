package net.zjvis.lab.nebula.crawler.core.processor;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.jackson.Jacksonized;
import net.zjvis.lab.nebula.crawler.core.page.DefaultPage;
import net.zjvis.lab.nebula.crawler.core.page.Page;
import org.jetbrains.annotations.NotNull;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;
import java.util.stream.Collectors;

@Builder(toBuilder = true)
@Jacksonized
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class DefaultProcessor extends AbstractProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultProcessor.class);

    @Override
    public boolean canProcess(Page page) {
        return !page.processed();
    }

    @NotNull
    protected Set<Page> doProcess(Page page) {
        String rawHTML = page.getContent();
        Document document = Jsoup.parse(rawHTML, page.getUrl());
        return document.select("a")
                .stream()
                .map(element -> element.attr("abs:href"))
                .map(link -> DefaultPage.builder()
                        .url(link)
                        .build())
                .collect(Collectors.toSet());
    }
}
