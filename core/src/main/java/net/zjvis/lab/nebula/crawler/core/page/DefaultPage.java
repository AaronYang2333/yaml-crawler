package net.zjvis.lab.nebula.crawler.core.page;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;
import lombok.extern.jackson.Jacksonized;
import okhttp3.OkHttpClient;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class DefaultPage extends AbstractPage {
    @Builder
    @Jacksonized
    public DefaultPage(@NonNull String url, OkHttpClient okHttpClient) {
        super(url, okHttpClient);
    }
}
