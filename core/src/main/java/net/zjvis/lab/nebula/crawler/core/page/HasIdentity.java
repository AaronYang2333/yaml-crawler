package net.zjvis.lab.nebula.crawler.core.page;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.google.common.base.Preconditions;

@JsonTypeInfo(property = "@class", use = JsonTypeInfo.Id.CLASS)
public interface HasIdentity<IdentityType> {
    IdentityType identity();

    default <T extends HasIdentity<?>> T unwrapHasIdentity(Class<T> clazz) {
        Preconditions.checkArgument(
                clazz.isInstance(this),
                String.format("hasIdentity(class=%s) is not instance of %s", this.getClass(), clazz.getName())
        );
        return clazz.cast(this);
    }
}
