package net.zjvis.lab.nebula.crawler.core.processor;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.zjvis.lab.nebula.crawler.core.page.Page;
import net.zjvis.lab.nebula.crawler.core.util.CopyAble;

import java.util.Set;

@JsonTypeInfo(property = "@class", use = JsonTypeInfo.Id.CLASS)
public interface Processor extends CopyAble {
    ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    Set<Page> process(Page page) throws Exception;

    boolean canProcess(Page page);
}
