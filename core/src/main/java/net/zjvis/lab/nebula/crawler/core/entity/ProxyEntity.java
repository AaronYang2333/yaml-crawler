package net.zjvis.lab.nebula.crawler.core.entity;

import lombok.*;
import lombok.extern.jackson.Jacksonized;

@Setter
@Getter
@Jacksonized
@EqualsAndHashCode
@ToString
@NoArgsConstructor
public class ProxyEntity {
    private String host;
    private String port;
    private String username;
    private String password;
}
