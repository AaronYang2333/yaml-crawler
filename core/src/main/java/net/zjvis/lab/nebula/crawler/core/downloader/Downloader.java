package net.zjvis.lab.nebula.crawler.core.downloader;

import java.io.IOException;
import java.io.OutputStream;

public interface Downloader {
    void download(String url, OutputStream outputStream) throws IOException;

    byte[] download(String url) throws IOException;
}
