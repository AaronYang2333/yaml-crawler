package net.zjvis.lab.nebula.crawler.core.page.persist;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.jackson.Jacksonized;
import net.zjvis.lab.nebula.crawler.core.component.ApplicationContextUtil;
import net.zjvis.lab.nebula.crawler.core.page.DownloadPage;
import net.zjvis.lab.nebula.crawler.core.page.HasIdentity;
import net.zjvis.lab.nebula.crawler.core.page.persist.entity.DownloadPageEntity;
import net.zjvis.lab.nebula.crawler.core.repository.DownloadPageRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Optional;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class DownloadPageDatabasePersist extends DownloadPagePersist {
    @JsonProperty
    private final String downloadPageRepositoryBeanName;
    private transient DownloadPageRepository downloadPageRepository;

    @Builder(toBuilder = true)
    @Jacksonized
    public DownloadPageDatabasePersist(String downloadPageRepositoryBeanName) {
        this.downloadPageRepositoryBeanName = null == downloadPageRepositoryBeanName
                ? DownloadPageRepository.class.getName()
                : downloadPageRepositoryBeanName;
    }

    @Override
    public HasIdentity<DownloadPage.ID> save(HasIdentity<DownloadPage.ID> page) {
        return downloadPageRepository().save(unwrap(page));
    }

    @Override
    public void deleteById(DownloadPage.ID id) {
        downloadPageRepository()
                .deleteByApplicationAndGroupKeyAndEntityKey(
                        id.getApplication(), id.getGroupKey(), id.getEntityKey());
    }

    @Override
    public HasIdentity<DownloadPage.ID> upsert(HasIdentity<DownloadPage.ID> page) {
        DownloadPageEntity downloadPage = unwrap(page);
        Optional<HasIdentity<DownloadPage.ID>> optional = findById(downloadPage.identity());
        return save(optional
                .map(this::unwrap)
                .map(downloadPageEntity -> downloadPageEntity.toBuilder()
                        .url(downloadPage.getUrl())
                        .dataJson(downloadPage.getDataJson())
                        .fileContent(downloadPage.getFileContent())
                        .timestamp(downloadPage.getTimestamp())
                        .build())
                .orElse(downloadPage));
    }

    @Override
    public Optional<HasIdentity<DownloadPage.ID>> findById(DownloadPage.ID id) {
        return downloadPageRepository()
                .findByApplicationAndGroupKeyAndEntityKey(
                        id.getApplication(), id.getGroupKey(), id.getEntityKey())
                .map(optional -> optional);
    }

    @Override
    public Page<HasIdentity<DownloadPage.ID>> findAll(Pageable pageable) {
        Page<DownloadPageEntity> page = downloadPageRepository().findAll(pageable);
        return new PageImpl<>(
                new ArrayList<>(page.getContent()),
                page.getPageable(),
                page.getTotalElements()
        );
    }

    private DownloadPageRepository downloadPageRepository() {
        if (null == downloadPageRepository) {
            downloadPageRepository = ApplicationContextUtil.getBean(
                    downloadPageRepositoryBeanName, DownloadPageRepository.class);
        }
        return downloadPageRepository;
    }
}
