package net.zjvis.lab.nebula.crawler.core.page;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import okhttp3.*;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@EqualsAndHashCode
@ToString
@JsonTypeInfo(property = "@class", use = JsonTypeInfo.Id.CLASS)
public abstract class AbstractPage implements Page, HttpPage {
    @Setter
    @Getter
    @JsonProperty
    private String url;
    @JsonProperty
    private final Map<String, String> formData;
    @Setter
    @JsonProperty
    private int weight;
    @EqualsAndHashCode.Exclude
    @Setter
    @Getter
    @JsonIgnore
    private OkHttpClient okHttpClient;
    @EqualsAndHashCode.Exclude
    @Setter
    @JsonIgnore
    private boolean processed;
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    private transient String content;

    public AbstractPage(String url) {
        this(url, null, null);
    }

    public AbstractPage(String url, Map<String, String> formData) {
        this(url, formData, null);
    }

    public AbstractPage(String url, OkHttpClient okHttpClient) {
        this(url, null, okHttpClient);
    }

    public AbstractPage(String url, Map<String, String> formData, OkHttpClient okHttpClient) {
        this.url = url;
        this.formData = formData;
        this.okHttpClient = null == okHttpClient
                ? new OkHttpClient.Builder()
                .connectTimeout(300, TimeUnit.SECONDS)
                .writeTimeout(300, TimeUnit.SECONDS)
                .readTimeout(300, TimeUnit.SECONDS)
                .build()
                : okHttpClient;
        processed = false;
        weight = 1;
    }

    @Override
    public String getContent() {
        if (null == content) {
            content = lazyFetch();
        }
        return content;
    }

    @Override
    public Map<String, String> getFormData() {
        return formData;
    }

    @Override
    public void close() {
        if (null != content) {
            content = null;
        }
    }

    @Override
    public int getWeight() {
        return weight;
    }

    @Override
    public boolean processed() {
        return processed;
    }

    @Override
    public int compareTo(@NotNull Object o) {
        return this.compareTo((Page) o);
    }

    protected String lazyFetch() {
        Request request = null;
        if (null == formData) {
            request = new Request.Builder()
                    .url(url)
                    .build();

        } else {
            FormBody.Builder formBodyBuilder = new FormBody.Builder();
            formData.forEach(formBodyBuilder::add);
            request = new Request.Builder()
                    .url(url)
                    .post(formBodyBuilder.build())
                    .build();
        }

        try (Response response = okHttpClient.newCall(request)
                .execute()) {
            ResponseBody body = response.body();
            if (null == body) {
                throw new RuntimeException("body is empty");
            }
            if (response.code() != 200) {
                throw new RuntimeException(String.format("failed to retrieve url [%s], since code: %s, message: %s", url, response.code(), response.message()));
            }
            return body.string();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
