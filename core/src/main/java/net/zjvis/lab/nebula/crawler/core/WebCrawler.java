package net.zjvis.lab.nebula.crawler.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import com.google.common.util.concurrent.MoreExecutors;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Singular;
import lombok.extern.jackson.Jacksonized;
import net.zjvis.lab.nebula.crawler.core.exception.BugException;
import net.zjvis.lab.nebula.crawler.core.page.Page;
import net.zjvis.lab.nebula.crawler.core.processor.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UncheckedIOException;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@EqualsAndHashCode
public class WebCrawler {
    private static class ProcessorRunnable implements Runnable {
        private final List<Processor> processorChain;
        private final PriorityBlockingQueue<Page> pageQueue;
        private final Page page;

        @Builder
        public ProcessorRunnable(
                List<Processor> processorChain,
                PriorityBlockingQueue<Page> pageQueue,
                Page page
        ) {
            this.processorChain = processorChain;
            this.pageQueue = pageQueue;
            this.page = page;
        }

        @Override
        public void run() {
            try {
                for (Processor processor : processorChain) {
                    if (processor.canProcess(page)) {
                        pageQueue.addAll(processor.process(page));
                        return;
                    }
                }
                LOGGER.warn("page({}) cannot be processed by any processor in processorChain({})",
                        page, processorChain);
            } catch (Exception e) {
                LOGGER.warn("process page({}) failed: {}", page, e.getMessage());
            }
        }
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(WebCrawler.class);
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper(YAMLFactory.builder()
            .disable(YAMLGenerator.Feature.USE_NATIVE_TYPE_ID)
            .build());
    @EqualsAndHashCode.Exclude
    @JsonIgnore
    private final BlockingQueue<Runnable> threadPoolQueue;
    @EqualsAndHashCode.Exclude
    @JsonIgnore
    private final PriorityBlockingQueue<Page> pageQueue;
    @EqualsAndHashCode.Exclude
    @JsonIgnore
    private final Set<Page> visitedPageSet;
    @EqualsAndHashCode.Exclude
    @JsonIgnore
    private final ExecutorService executorService;
    @JsonProperty
    private final List<Processor> processorChain;
    @JsonProperty
    private final Integer nThreads;
    @JsonProperty
    private final long pollWaitingTime;
    @JsonProperty
    private final TimeUnit pollWaitingTimeUnit;
    @JsonProperty
    private long waitFinishedTimeout;
    @JsonProperty
    private TimeUnit waitFinishedTimeUnit;
    private final AtomicInteger count = new AtomicInteger(0);

    @Builder
    @Jacksonized
    public WebCrawler(
            @Singular("processor") List<Processor> processorChain,
            Integer nThreads,
            Long pollWaitingTime,
            TimeUnit pollWaitingTimeUnit,
            Long waitFinishedTimeout,
            TimeUnit waitFinishedTimeUnit
    ) {
        this.processorChain = processorChain;
        this.nThreads = null == nThreads ? 1 : nThreads;
        this.pollWaitingTime = null == pollWaitingTime ? 30 : pollWaitingTime;
        this.pollWaitingTimeUnit = null == pollWaitingTimeUnit ? TimeUnit.SECONDS : pollWaitingTimeUnit;
        this.waitFinishedTimeout = null == waitFinishedTimeout ? 1800L : waitFinishedTimeout;
        this.waitFinishedTimeUnit = null == waitFinishedTimeUnit ? TimeUnit.SECONDS : waitFinishedTimeUnit;
        threadPoolQueue = new LinkedBlockingQueue<>();
        pageQueue = new PriorityBlockingQueue<>();
        visitedPageSet = ConcurrentHashMap.newKeySet();
        ExecutorService rawExecutorService = new ThreadPoolExecutor(
                this.nThreads, this.nThreads, 0L, TimeUnit.MILLISECONDS, threadPoolQueue);
        if (!(rawExecutorService instanceof ThreadPoolExecutor)) {
            throw new BugException(
                    String.format(
                            "rawExecutorService(class=%s) is not a %s",
                            rawExecutorService.getClass().getName(), ThreadPoolExecutor.class.getName()));
        }
        // TODO log warn
        this.executorService = MoreExecutors.getExitingExecutorService(
                (ThreadPoolExecutor) rawExecutorService, this.waitFinishedTimeout, this.waitFinishedTimeUnit);
    }

    public void crawl(Collection<Page> rootPages) {
        try {
            LOGGER.info("crawler: \n{}", OBJECT_MAPPER.writeValueAsString(this));
            // TODO bug of jackson ?
            // LOGGER.info("rootPages: \n{}", OBJECT_MAPPER.writeValueAsString(rootPages));
            LOGGER.info("rootPages: \n{}", rootPages.stream()
                    .map(page -> {
                        try {
                            return OBJECT_MAPPER.writeValueAsString(page);
                        } catch (JsonProcessingException e) {
                            throw new UncheckedIOException(e);
                        }
                    }).collect(Collectors.joining("\n")));
        } catch (JsonProcessingException e) {
            throw new UncheckedIOException(e);
        }
        pageQueue.addAll(rootPages);
        Page page;
        try {
            while (null != (page = pageQueue.poll(pollWaitingTime, pollWaitingTimeUnit))
                    || !threadPoolQueue.isEmpty()) {
                if (null == page) {
                    continue;
                }
                if (notVisited(page)) {
                    executorService.submit(ProcessorRunnable.builder()
                            .processorChain(processorChain.stream()
                                    .map(processor -> (Processor) processor.copy())
                                    .collect(Collectors.toList()))
                            .pageQueue(pageQueue)
                            .page(page)
                            .build());
                    visitedPageSet.add(page);
                }
            }
            // TODO remove
            this.executorService.shutdown();
            boolean terminated = this.executorService.awaitTermination(waitFinishedTimeout, waitFinishedTimeUnit);
            if (!terminated) {
                LOGGER.warn("shutdown before all thread finished, but have waited {} {}",
                        waitFinishedTimeout, waitFinishedTimeUnit);
            }
        } catch (InterruptedException e) {
            LOGGER.error("interrupted while polling from queue", e);
        }
    }

    protected boolean notVisited(Page page) {
        return !visitedPageSet.contains(page);
    }
}