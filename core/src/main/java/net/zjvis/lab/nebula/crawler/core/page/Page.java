package net.zjvis.lab.nebula.crawler.core.page;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.google.common.base.Preconditions;

import java.util.Map;

@JsonTypeInfo(property = "@class", use = JsonTypeInfo.Id.CLASS)
public interface Page extends AutoCloseable, Comparable {
    String getUrl();

    String getContent();

    int getWeight();

    Map<String, String> getFormData();

    void setProcessed(boolean processed);

    boolean processed();

    default <T extends Page> T unwrap(Class<? extends T> clazz) {
        Preconditions.checkArgument(
                clazz.isInstance(this),
                String.format("page(class=%s) is not instance of %s", this.getClass(), clazz.getName())
        );
        return clazz.cast(this);
    }

    default <T extends Page> int compareTo(T obj) {
        return this.getWeight() > obj.getWeight() ? 0 : 1;
    }
}
