package net.zjvis.lab.nebula.crawler.core.component;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class BeanFactory {
    @Bean
    public String webCrawlerConfigurationPath(@Value("${application.webCrawler.configuration.path:not set}") String path) {
        return path;
    }

    @Bean
    public String webCrawlerRootPagesPath(@Value("${application.webCrawler.rootPages.path:not set}") String path) {
        return path;
    }
}
