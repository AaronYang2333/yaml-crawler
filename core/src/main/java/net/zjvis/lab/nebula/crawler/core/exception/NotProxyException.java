package net.zjvis.lab.nebula.crawler.core.exception;

public class NotProxyException extends RuntimeException {
    public NotProxyException(String message) {
        super(message);
    }

    public NotProxyException(String message, Throwable cause) {
        super(message, cause);
    }
}
