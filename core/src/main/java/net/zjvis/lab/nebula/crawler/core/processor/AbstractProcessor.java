package net.zjvis.lab.nebula.crawler.core.processor;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.zjvis.lab.nebula.crawler.core.page.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

@EqualsAndHashCode
@ToString
public abstract class AbstractProcessor implements Processor {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultProcessor.class);

    @Override
    public boolean canProcess(Page page) {
        return !page.processed();
    }

    @Override
    public Set<Page> process(Page page) throws Exception {
        LOGGER.info("processing {}", page);
        Set<Page> pageSetToTraverse = doProcess(page);
        page.setProcessed(true);
        return pageSetToTraverse;
    }

    protected abstract Set<Page> doProcess(Page page) throws Exception;
}
