package net.zjvis.lab.nebula.crawler.core.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.extern.jackson.Jacksonized;
import okhttp3.OkHttpClient;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Getter
public class DatasetPage<DataType> extends AbstractPage implements HasIdentity<DatasetPage.ID> {
    @Getter
    @EqualsAndHashCode
    @ToString
    public static class ID {
        private final String application;
        private final String entityKey;

        @Builder
        public ID(
                String application,
                String entityKey
        ) {
            this.application = application;
            this.entityKey = entityKey;
        }
    }

    @JsonProperty
    private final String application;
    @JsonProperty
    private final String entityKey;
    @JsonProperty
    private final DataType data;
    @JsonProperty
    private final Long timestamp;
    @Builder.Default
    @JsonProperty
    private final int weight = 10;

    @Builder
    @Jacksonized
    public DatasetPage(
            @NonNull String url,
            @NonNull String application,
            @NonNull String entityKey,
            DataType data,
            Long timestamp,
            OkHttpClient okHttpClient
    ) {
        super(url, okHttpClient);
        this.application = application;
        this.entityKey = entityKey;
        this.data = data;
        this.timestamp = timestamp;
    }

    @Override
    public ID identity() {
        return ID.builder()
                .application(application)
                .entityKey(entityKey)
                .build();
    }
}
