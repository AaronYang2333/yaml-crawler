package net.zjvis.lab.nebula.crawler.core.page.persist;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.zjvis.lab.nebula.crawler.core.repository.DownloadPageRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.testcontainers.shaded.org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.util.stream.Stream;

public class PagePersistTest {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static File persistBaseDirectory;

    @BeforeAll
    static void beforeAll() throws IOException {
        persistBaseDirectory = Files.createTempDirectory("persist-").toFile();
    }

    @AfterAll
    static void afterAll() {
        if (null != persistBaseDirectory) {
            FileUtils.deleteQuietly(persistBaseDirectory);
        }
    }

    @Test
    void testSerialization() {
        Stream.of(
                DownloadPageDatabasePersist.builder()
                        .downloadPageRepositoryBeanName(DownloadPageRepository.BEAN_NAME)
                        .build(),
                DownloadPageFileSystemPersist.builder()
                        .baseDirectory(persistBaseDirectory)
                        .build()
        ).forEach(this::assertSerialization);
    }

    private void assertSerialization(PagePersist<?> pagePersist) {
        try {
            Assertions.assertEquals(
                    pagePersist,
                    OBJECT_MAPPER.readValue(
                            OBJECT_MAPPER.writeValueAsString(pagePersist),
                            new TypeReference<PagePersist<?>>() {
                            })
            );
        } catch (JsonProcessingException e) {
            throw new UncheckedIOException(e);
        }
    }
}
