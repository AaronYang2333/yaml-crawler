package net.zjvis.lab.nebula.crawler.core.util;

import lombok.Builder;
import net.zjvis.lab.nebula.crawler.core.page.DefaultPage;
import net.zjvis.lab.nebula.crawler.core.page.Page;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Builder
public class PageBox {

    public Page generatePage(String url) {
        return DefaultPage.builder()
                .url(url)
                .build();
    }

    public Set<Page> generatePageSet(String url) {
        return IntStream.range(0, 10)
                .mapToObj(index -> generatePage(url))
                .collect(Collectors.toSet());
    }
}
