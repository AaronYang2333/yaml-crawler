package net.zjvis.lab.nebula.crawler.core.page;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.UncheckedIOException;
import java.util.stream.Stream;

public class PageTest {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper(YAMLFactory.builder()
            .disable(YAMLGenerator.Feature.USE_NATIVE_TYPE_ID)
            .build());

    @Test
    void testSerialization() {
        Stream.of(
                DefaultPage.builder()
                        .url("https://github.com")
                        .build(),
                DownloadPage.builder()
                        .url("https://github.com")
                        .application("application_id")
                        .groupKey("group-key")
                        .entityKey("entity-key")
                        .filename("file-name")
                        .data("{\"some\": \"json\"}")
                        .timestamp(System.currentTimeMillis())
                        .build()
        ).forEach(this::assertSerialization);
    }

    private void assertSerialization(Page page) {
        try {
            Assertions.assertEquals(
                    page,
                    OBJECT_MAPPER.readValue(
                            OBJECT_MAPPER.writeValueAsString(page),
                            new TypeReference<Page>() {
                            })
            );
        } catch (JsonProcessingException e) {
            throw new UncheckedIOException(e);
        }
    }
}
