package net.zjvis.lab.nebula.crawler.core;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import net.zjvis.lab.nebula.crawler.core.page.DownloadPage;
import net.zjvis.lab.nebula.crawler.core.page.persist.DownloadPageDatabasePersist;
import net.zjvis.lab.nebula.crawler.core.processor.decorator.RetryProxyControl;
import net.zjvis.lab.nebula.crawler.core.processor.decorator.SpeedControl;
import net.zjvis.lab.nebula.crawler.core.processor.download.DefaultDownloadPageTransformer;
import net.zjvis.lab.nebula.crawler.core.processor.download.DownloadProcessor;
import net.zjvis.lab.nebula.crawler.core.processor.download.SkipExistsById;
import net.zjvis.lab.nebula.crawler.core.repository.DownloadPageRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.UncheckedIOException;
import java.util.concurrent.TimeUnit;

public class WebCrawlerTest {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper(
            new YAMLFactory().disable(YAMLGenerator.Feature.USE_NATIVE_TYPE_ID));

    @Test
    void testSerialization() throws JsonProcessingException {
        WebCrawler crawler = WebCrawler.builder()
                .processor(RetryProxyControl.builder()
                        .processor(SpeedControl.builder()
                                .processor(DownloadProcessor.<DownloadPage.ID>builder()
                                        .pagePersist(DownloadPageDatabasePersist.builder()
                                                .downloadPageRepositoryBeanName(DownloadPageRepository.BEAN_NAME)
                                                .build())
                                        .downloadPageTransformer(DefaultDownloadPageTransformer
                                                .<DownloadPage.ID>builder()
                                                .build())
                                        .skipExists(SkipExistsById.<DownloadPage.ID>builder().build())
                                        .build())
                                .time(1L)
                                .unit(TimeUnit.SECONDS)
                                .build())
                        .retryTimes(3)
                        .build())
                .build();
        System.out.println(OBJECT_MAPPER.writeValueAsString(crawler));
        try {
            Assertions.assertEquals(
                    crawler,
                    OBJECT_MAPPER.readValue(
                            OBJECT_MAPPER.writeValueAsString(crawler),
                            new TypeReference<WebCrawler>() {
                            })
            );
        } catch (JsonProcessingException e) {
            throw new UncheckedIOException(e);
        }
    }
}
