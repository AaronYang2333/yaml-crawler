package net.zjvis.lab.nebula.crawler.core.processor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.zjvis.lab.nebula.crawler.core.page.DefaultPage;
import net.zjvis.lab.nebula.crawler.core.page.DownloadPage;
import net.zjvis.lab.nebula.crawler.core.page.Page;
import net.zjvis.lab.nebula.crawler.core.page.persist.DownloadPageFileSystemPersist;
import net.zjvis.lab.nebula.crawler.core.processor.decorator.ExceptionRecord;
import net.zjvis.lab.nebula.crawler.core.processor.decorator.RetryProxyControl;
import net.zjvis.lab.nebula.crawler.core.processor.decorator.SpeedControl;
import net.zjvis.lab.nebula.crawler.core.processor.download.DefaultDownloadPageTransformer;
import net.zjvis.lab.nebula.crawler.core.processor.download.DownloadProcessor;
import net.zjvis.lab.nebula.crawler.core.processor.download.SkipExistsById;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.testcontainers.shaded.org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class ProcessorTest {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static File processingErrorLogFile;
    private static File persistBaseDirectory;

    @BeforeAll
    static void beforeAll() throws IOException {
        processingErrorLogFile = File.createTempFile("processing.", ".error");
        persistBaseDirectory = Files.createTempDirectory("persist-").toFile();
    }

    @AfterAll
    static void afterAll() {
        if (null != processingErrorLogFile) {
            FileUtils.deleteQuietly(processingErrorLogFile);
        }
        if (null != persistBaseDirectory) {
            FileUtils.deleteQuietly(persistBaseDirectory);
        }
    }


    @Test
    void testSerialization() {
        processingErrorLogFile.deleteOnExit();
        DefaultProcessor defaultProcessor = DefaultProcessor.builder().build();
        Stream.of(
                defaultProcessor,
                DownloadProcessor.<DownloadPage.ID>builder()
                        .downloadPageTransformer(DefaultDownloadPageTransformer.<DownloadPage.ID>builder().build())
                        .pagePersist(DownloadPageFileSystemPersist.builder()
                                .baseDirectory(persistBaseDirectory)
                                .build())
                        .skipExists(SkipExistsById.<DownloadPage.ID>builder().build())
                        .build(),
                ExceptionRecord.builder()
                        .processor(defaultProcessor)
                        .processingErrorLogFile(processingErrorLogFile)
                        .build(),
                SpeedControl.builder()
                        .processor(defaultProcessor)
                        .unit(TimeUnit.SECONDS)
                        .time(1L)
                        .build(),
                RetryProxyControl.builder()
                        .processor(defaultProcessor)
                        .retryTimes(3)
                        .build()
        ).forEach(this::assertSerialization);
    }

    @Test
    void testProxy() throws Exception {
        String ip = "125.87.93.40:22009";
        processingErrorLogFile.deleteOnExit();
        DefaultProcessor defaultProcessor = DefaultProcessor.builder().build();
        RetryProxyControl retryProxyControl = RetryProxyControl.builder()
                .processor(defaultProcessor)
                .retryTimes(3)
                .build();
        DefaultPage defaultPage = DefaultPage.builder().url("http://ip-api.com/json/?lang=zh-CN").build();
        Set<Page> Page = retryProxyControl.process(defaultPage);
        String rawHTML =  defaultPage.getContent();
        Assertions.assertEquals( rawHTML.substring(rawHTML.lastIndexOf(":")+2,rawHTML.length()-2),ip.substring(0,ip.lastIndexOf(":")));
    }

    private void assertSerialization(Processor processor) {
        try {
            Assertions.assertEquals(
                    processor,
                    OBJECT_MAPPER.readValue(
                            OBJECT_MAPPER.writeValueAsString(processor),
                            new TypeReference<Processor>() {
                            })
            );
        } catch (JsonProcessingException e) {
            throw new UncheckedIOException(e);
        }
    }
}
