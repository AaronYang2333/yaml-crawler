package net.zjvis.lab.nebula.crawler.core.processor;

import net.zjvis.lab.nebula.crawler.core.page.DefaultPage;
import net.zjvis.lab.nebula.crawler.core.page.Page;
import net.zjvis.lab.nebula.crawler.core.util.PageBox;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Set;
import java.util.stream.Collectors;

public class DefaultProcessorTest {
    private static final String ROOTURL = "https://gitee.com/";
    private final DefaultProcessor defaultProcessorMock = Mockito.mock(DefaultProcessor.class);
    private Page page;
    private Set<Page> pageSet;

    @BeforeEach
    void setUp() {
        PageBox pageBox = PageBox.builder().build();
        page = pageBox.generatePage(ROOTURL);
        pageSet = pageBox.generatePageSet(ROOTURL);
    }

    @Test
    void testProcess() throws Exception {
        Mockito.when(defaultProcessorMock.doProcess(Mockito.eq(page)))
                .thenReturn(pageSet);
        Mockito.doCallRealMethod().when(defaultProcessorMock)
                .process(Mockito.any());
        Set<Page> pageSetToTraverse = defaultProcessorMock.process(page);
        Assertions.assertTrue(page.processed());
        Assertions.assertEquals(pageSet, pageSetToTraverse);
        Mockito.verify(defaultProcessorMock).process(page);
        Mockito.clearInvocations(defaultProcessorMock);
    }

    @Test
    void testDeepCopy() {
        Mockito.doCallRealMethod().when(defaultProcessorMock).copy();
        Mockito.doCallRealMethod().when(defaultProcessorMock).toBuilder();
        Assertions.assertEquals(defaultProcessorMock.toBuilder().build(), defaultProcessorMock.copy());
        Mockito.verify(defaultProcessorMock).copy();
        Mockito.clearInvocations(defaultProcessorMock);
    }

    @Test
    void testDoProcess() {
        Mockito.doCallRealMethod().when(defaultProcessorMock).doProcess(page);
        Assertions.assertEquals(results(page), defaultProcessorMock.doProcess(page));
        Mockito.verify(defaultProcessorMock).doProcess(page);
        Mockito.clearInvocations(defaultProcessorMock);
    }

    private Set<Page> results(Page page) {
        String rawHTML = page.getContent();
        Document document = Jsoup.parse(rawHTML, page.getUrl());
        return document.select("a")
                .stream()
                .map(element -> element.attr("abs:href"))
                .map(link -> DefaultPage.builder().url(link).build())
                .collect(Collectors.toSet());
    }
}
